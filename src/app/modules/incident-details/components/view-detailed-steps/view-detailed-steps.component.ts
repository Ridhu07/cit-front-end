import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-detailed-steps',
  templateUrl: './view-detailed-steps.component.html',
  styleUrls: ['./view-detailed-steps.component.scss']
})
export class ViewDetailedStepsComponent implements OnInit{


  public incidentForm: FormGroup | any;
  constructor(private routes: Router, private fb: FormBuilder) {}
  
  stepArray: any[] = [
   
  ];
  incidentDetails = [

    {

      "incidentId": 'INCI12',

      "incidentDescription": "401 Authorization Error",

      "suggestedSteps": [

          "{ Open the API gateway console, please click here to open.}",

          "{ Choose the name of the API.}",

          "{In the navigation pane, choose the authorizers under the API.}",

          "{Review the list of authorizer's present. If not available, please add the remarks and close the ticket.}"

      ],

      "remarks": "",
      status: "open",
      time: '08-08-2023',
      short: "pdp not working properly"

  },

    // {

    //     "incidentId": 2,

    //     "incidentDescription": "500 internal server error",

    //     "suggestedSteps": [

    //         "{Reload the page.}",

    //         "{ Clear you browser cache and cookies.}",

    //         "{Please click here see the error logs in the server.}",

    //         "{Validate the input request is properly formatted as per the api documentation, please click here for api documentation.}"

    //     ],

    //     "remarks": "",
    //     status: "open"

    // }

]
  arr: any;

  ngOnInit(): void {
    this.stepArray = [];
  this.stepArray = this.incidentDetails.map((res: any) => {
    console.log(res.suggestedSteps)
    return res.suggestedSteps

   
  })
  console.log(this.stepArray)
  }

}
