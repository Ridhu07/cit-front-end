export interface Incidents {
    incidentId:string;
    active:string;
    state:string;
    priority:string;
    openedDate:string;
    assignedTo:string;
}
